# OrganizationLoader

Commandline application which downloads the IDM exports and parses it to a .ttl file.

It has the following parameters
- force=: Skip checks for to many or no changes in the exports.
- username=: Username for the export. If none is provided, the consul value is used.
- password=: Password for the export. If none is provided, the consul value is used.
- organizationsLink=: Link for the organizations export. If none is provided, the consul value is used.
- employeesLink=: Link for the employee export. If none is provided, the consul value is used.
- rorId=: The RWTH ror Id. If none is provided, the consul value is used.
- output=: Output for the generated file (Mandatory).
- h|help: show help and exit

To show the above output, run: `.\OrganizationLoader.exe --h`

Create a .ttl file with: `.\OrganizationLoader.exe --output="index.ttl"`

Should a parameter be empty or not provided, the program will look it up in Consul.
Only the `output` parameter has to be supplied via the commandline.
The other configuration parametes must be provided by the commandline or by Consul.

Commandline parameters have a higher priority and are choosen instead of Consul values.

# Gitlab CI

Most commandline parameters can be set over the variables section of the Gitlab project.
The help and output parameter are unused or fixed.

The scheduled CI task runs every morning at 1am.
The task builds the executable and runs it automatically.