﻿using Coscine.Configuration;
using NDesk.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Coscine.OrganizationLoader
{
    public class Program
    {
        private static HttpClient _httpClient;

        private static readonly List<ResourceType> _resourceTypes = new List<ResourceType>
        {
            new ResourceType
            {
                Name ="rds",
                Quota = 25,
                MaxQuota = 100,
                HasQuota = true,
            },
            new ResourceType
            {  Name ="rdss3",
                Quota = 0,
                MaxQuota = 0,
                HasQuota = true,
            },
            new ResourceType
            {  Name = "linked",
               Quota = -1,
               MaxQuota = -1,
               HasQuota = false,
            },
        };

        // taken from https://tools.aai.dfn.de/entities/ and https://git.rwth-aachen.de/coscine/graphs/organizations/-/raw/master/ROR/index.ttl
        private static readonly List<OrganizationMapping> _organizationMappings = new List<OrganizationMapping>
        {
            new OrganizationMapping
            {
                RorId = "https://ror.org/04xfq0f34",
                EntityId = "https://login.rz.rwth-aachen.de/shibboleth",
            },
            new OrganizationMapping
            {
                RorId = "https://ror.org/04xfq0f34",
                EntityId = "https://login-test.rz.rwth-aachen.de/shibboleth",
            },
        };

        static void Main(string[] args)
        {
            bool showHelp = false;
            bool force = false;
            string username = null;
            string password = null;
            string organizationsLink = null;
            string employeesLink = null;
            string rorId = null;
            string output = null;

            var optionSet = new OptionSet() {
                { "force=",  "Skip checks for to many or no changes in the exports.",
                   x => force = x != null && x == "true" },
                { "username=",  "Username for the export. If none is provided, the consul value is used.",
                   x => username = x },
                { "password=",  "Password for the export. If none is provided, the consul value is used.",
                   x => password = x },
                { "organizationsLink=",  "Link for the organizations export. If none is provided, the consul value is used.",
                   x => organizationsLink = x },
                { "employeesLink=",  "Link for the employee export. If none is provided, the consul value is used.",
                   x => employeesLink = x },
                { "rorId=",  "The RWTH ror Id. If none is provided, the consul value is used.",
                   x => rorId = x },
                { "output=",  "Output for the generated file (Mandatory).",
                   x => output = x },
                { "h|help",  "show help and exit",
                   x => showHelp = x != null },
            };

            List<string> extra;
            try
            {
                extra = optionSet.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try '--help' for more information.");
                return;
            }

            if (showHelp)
            {
                foreach (var option in optionSet)
                {
                    Console.WriteLine($"{option}: {option.Description}");
                }
                return;
            }

            if (force)
            {
                Console.WriteLine("Forced execution! Checks will be skipped.");
            }

            if (string.IsNullOrWhiteSpace(output))
            {
                Console.WriteLine("No value for output provided.");
                return;
            }

            var configuration = new ConsulConfiguration();

            rorId = string.IsNullOrWhiteSpace(rorId) ? configuration.GetString("coscine/global/organizations/rwth/ror_url") : rorId;
            if (string.IsNullOrWhiteSpace(rorId))
            {
                Console.WriteLine("No value for rorId provided.");
            }

            organizationsLink = string.IsNullOrWhiteSpace(organizationsLink) ? configuration.GetString("coscine/global/organizations/rwth/files/organizations/link") : organizationsLink;
            if (string.IsNullOrWhiteSpace(organizationsLink))
            {
                Console.WriteLine("No value for organizationsLink provided.");
            }

            employeesLink = string.IsNullOrWhiteSpace(employeesLink) ? configuration.GetString("coscine/global/organizations/rwth/files/employees/link") : employeesLink;
            if (string.IsNullOrWhiteSpace(employeesLink))
            {
                Console.WriteLine("No value for employeesLink provided.");
            }

            username = string.IsNullOrWhiteSpace(username) ? configuration.GetString("coscine/global/organizations/rwth/connection_data/username") : username;
            if (string.IsNullOrWhiteSpace(username))
            {
                Console.WriteLine("No value for username provided.");
            }

            password = string.IsNullOrWhiteSpace(password) ? configuration.GetString("coscine/global/organizations/rwth/connection_data/password") : password;
            if (string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("No value for password provided.");
            }

            _httpClient = new HttpClient();
            // Create a basic Authentication header
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));

            var previousEmployeeCountString = configuration.GetString("coscine/global/organizations/rwth/files/employees/number_of_entries");
            var previousEmployeeCount = previousEmployeeCountString != null ? int.Parse(previousEmployeeCountString) : -1;

            var previousOrganizationsCountString = configuration.GetString("coscine/global/organizations/rwth/files/organizations/number_of_entries");
            var previousOrganizationsCount = previousOrganizationsCountString != null ? int.Parse(previousOrganizationsCountString) : -1;

            var previousEmployeeSha = configuration.GetString("coscine/global/organizations/rwth/files/employees/sha");
            var previousOrganizationsSha = configuration.GetString("coscine/global/organizations/rwth/files/organizations/sha");

            string organizationsString;
            string employeesString;

            try
            {
                Console.WriteLine("Downloading...");

                organizationsString = DownloadExport(organizationsLink);
                employeesString = DownloadExport(employeesLink);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to download exports: {e.Message}");
                return;
            }

            Console.WriteLine("Parsing...");

            var organizations = ParseOrganizations(organizationsString);
            var employees = LoadEmployees(employeesString, organizations);

            if (!force)
            {
                Console.WriteLine("Checking...");

                if (previousEmployeeCount > 0 && Math.Abs(previousEmployeeCount - employees.Count()) > previousEmployeeCount * 0.1f)
                {
                    Console.WriteLine($"To many employees changed! Previously: {previousEmployeeCount}, Now: {employees.Count()}, Difference: {Math.Abs(previousEmployeeCount - employees.Count())} or {((float)Math.Abs(previousEmployeeCount - employees.Count()) / previousEmployeeCount) * 100} %");
                    return;
                }
                if (previousOrganizationsCount > 0 && Math.Abs(previousOrganizationsCount - organizations.Count()) > previousOrganizationsCount * 0.1f)
                {
                    Console.WriteLine($"To many organizations changed! Previously: {previousOrganizationsCount}, Now: {organizations.Count()}, Difference: {Math.Abs(previousOrganizationsCount - organizations.Count())} or {((float)Math.Abs(previousOrganizationsCount - organizations.Count()) / previousOrganizationsCount) * 100} %");
                    return;
                }
            }

            var organizationsSha = Sha256Hash(organizationsString);
            var employeesSha = Sha256Hash(employeesString);

            if (!force && previousEmployeeSha != null && previousEmployeeSha == employeesSha && previousOrganizationsSha != null && previousOrganizationsSha == organizationsSha)
            {
                Console.WriteLine("The sha of the exports has not changed.");
                return;
            }

            Console.WriteLine("Generating ttl file...");

            var ttl = GenerateTurtle(organizations, employees, rorId);

            var file = new FileInfo(output);
            file.Directory.Create();
            File.WriteAllText(output, ttl);
            Console.WriteLine($"File written to {output}");

            Console.WriteLine("Saving to Consul...");

            configuration.Put("coscine/global/organizations/rwth/files/organizations/number_of_entries", organizations.Count().ToString());
            configuration.Put("coscine/global/organizations/rwth/files/employees/number_of_entries", employees.Count().ToString());
            configuration.Put("coscine/global/organizations/rwth/files/organizations/sha", organizationsSha);
            configuration.Put("coscine/global/organizations/rwth/files/employees/sha", employeesSha);

            Console.WriteLine("Finished");
        }

        public static string Sha256Hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                return string.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(value))
                  .Select(item => item.ToString("x2")));
            }
        }

        public static List<Employee> LoadEmployees(string xml, Dictionary<string, Organization> organizations)
        {
            var employeeDocument = new XmlDocument();

            employeeDocument.LoadXml(xml);

            var employees = new List<Employee>();

            foreach (XmlNode node in employeeDocument.SelectNodes("/IDM_Exports/Employees/Employee"))
            {
                var employee = new Employee
                {
                    Surname = node["Surname"].InnerText?.Trim(),
                    PreferredName = node["PreferredName"].InnerText?.Trim(),
                    ID = node["ID"].InnerText?.Trim(),
                    RONID = node["RONID"].InnerText?.Trim(),
                    Memberships = node["OrganizationMemberships"]?.ChildNodes?.Cast<XmlNode>().Select(x => x["OrgId"].InnerText?.Trim()).ToList(),
                    DegreePre = node["DegreePre"]?.InnerText,
                    DegreePost = node["DegreePost"]?.InnerText,
                    Ubpublications = node["Ubpublications"]?.InnerText?.Trim(),
                    RoleAssignments = node["RoleAssignments"]?.ChildNodes?.Cast<XmlNode>().Select(x => new Role { RID = x["RID"].InnerText?.Trim(), OrgId = x["OrgId"].InnerText?.Trim() }).ToList(),

                };

                employees.Add(employee);
                if (employee.Memberships != null)
                {
                    foreach (var orgId in employee.Memberships)
                    {
                        organizations[orgId].Members.Add(employee);
                    }
                }
            }

            return employees;
        }

        public static string DownloadExport(string link)
        {
            using (var response = _httpClient.GetStringAsync(link))
            {
                return response.Result;
            }
        }

        public static Dictionary<string, Organization> ParseOrganizations(string xml)
        {
            var organizationDocument = new XmlDocument();

            organizationDocument.LoadXml(xml);

            var organizations = new Dictionary<string, Organization>();

            foreach (XmlNode node in organizationDocument.SelectNodes("/IDM_Exports/Organizations/Organization"))
            {
                organizations.Add(node["OrgId"].InnerText,
                     new Organization
                     {
                         OrgId = node["OrgId"].InnerText?.Trim(),
                         // IDM changed this to capital and might change it back. Keeps us safe for now.
                         Name = (node["NAME"] ?? node["Name"]).InnerText?.Trim(),
                         IKZ = node["IKZ"].InnerText?.Trim(),
                         Abbreviation = node["Abbreviation"]?.InnerText?.Trim(),
                         CMSLink = node["CMSLink"].InnerText?.Trim(),
                     }
                 );
            }

            return organizations;
        }

        public static string GenerateTurtle(Dictionary<string, Organization> organizations, List<Employee> employees, string rorId)
        {
            // Disposed by streamWriter
            var memoryStream = new MemoryStream();

            using (var streamWriter = new StreamWriter(memoryStream))
            {
                // Header
                streamWriter.WriteLine($"@base <{rorId}> .");
                streamWriter.WriteLine();
                streamWriter.WriteLine(@"@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .");
                streamWriter.WriteLine(@"@prefix org: <http://www.w3.org/ns/org#> .");
                streamWriter.WriteLine(@"@prefix dcterms: <http://purl.org/dc/terms/> .");
                streamWriter.WriteLine(@"@prefix foaf: <http://xmlns.com/foaf/0.1/> .");
                streamWriter.WriteLine(@"@prefix coscineresource: <https://purl.org/coscine/terms/resource#> .");
                streamWriter.WriteLine();
                streamWriter.WriteLine($"<{rorId}>");
                streamWriter.WriteLine(@"    dcterms:publisher <https://itc.rwth-aachen.de/> ;");
                streamWriter.WriteLine(@"    dcterms:rights ""Copyright © 2023 IT Center, RWTH Aachen University"" ;");
                streamWriter.WriteLine(@"    dcterms:title ""RWTH Aachen University""@en ;");
                streamWriter.WriteLine();
                streamWriter.WriteLine(@"    a org:FormalOrganization ;");
                streamWriter.WriteLine(@"    rdfs:label ""RWTH Aachen University"" ;");

                foreach (var organization in organizations.Values)
                {
                    streamWriter.WriteLine($"    org:hasUnit <{rorId}#{organization.OrgId}> ;");
                }

                streamWriter.WriteLine(".");

                // Organizations
                foreach (var organization in organizations.Values)
                {
                    streamWriter.WriteLine($"<{rorId}#{organization.OrgId}>");
                    streamWriter.WriteLine($"   rdfs:label \"{organization.Name}\" ;");
                    streamWriter.WriteLine($"   org:identifier \"org-id:{organization.OrgId}\" ;");
                    streamWriter.WriteLine($"   org:identifier \"ikz:{organization.IKZ}\" ;");
                    streamWriter.WriteLine($"   a org:OrganizationalUnit ;");
                    streamWriter.WriteLine(".");
                }

                // Memberships
                foreach (var employee in employees)
                {
                    if (employee.Memberships != null)
                    {
                        foreach (var membership in employee.Memberships)
                        {
                            streamWriter.WriteLine("[] a org:Membership ;");
                            streamWriter.WriteLine($"    org:member <{rorId}#{employee.ID}> ;");
                            streamWriter.WriteLine($"    org:organization <{rorId}#{membership}> ;");
                            streamWriter.WriteLine(".");
                        }
                    }
                }

                // Persons
                foreach (var employee in employees)
                {
                    streamWriter.WriteLine($"<{rorId}#{employee.ID}>");
                    streamWriter.WriteLine($"    foaf:name \"{employee.PreferredName} {employee.Surname}\" ;");
                    streamWriter.WriteLine($"    foaf:givenName \"{employee.PreferredName}\" ;");
                    streamWriter.WriteLine($"    foaf:familyName \"{employee.Surname}\" ;");
                    streamWriter.WriteLine($"    foaf:openId \"{employee.ID}\" ;");
                    streamWriter.WriteLine($"    a foaf:Person ;");
                    streamWriter.WriteLine(".");
                }

                // Default Quoting
                foreach (var resourceType in _resourceTypes)
                {
                    streamWriter.WriteLine($"<{rorId}> coscineresource:typeSpecification [");
                    streamWriter.WriteLine($@"    coscineresource:type <https://purl.org/coscine/terms/resource/types#{resourceType.Name}>;");
                    if (resourceType.HasQuota)
                    {
                        streamWriter.WriteLine($@"    coscineresource:defaultQuota ""{resourceType.Quota}"";");
                        streamWriter.WriteLine($@"    coscineresource:defaultMaxQuota ""{resourceType.MaxQuota}"";");
                    }
                    streamWriter.WriteLine("].");
                }

                // Ror to entity mapping
                foreach (var organizationMapping in _organizationMappings)
                {
                    // Specs require the org identifier to be a literal, even if it is an url.
                    streamWriter.WriteLine($@"<{organizationMapping.RorId}> org:identifier ""{organizationMapping.EntityId}"" .");
                }

                // Contact Information
                streamWriter.WriteLine($"<{rorId}> coscineresource:contactInformation [");
                streamWriter.WriteLine(@"    foaf:name ""Service Desk"";");
                streamWriter.WriteLine(@"    foaf:mbox <mailto:servicedesk@rwth-aachen.de>;");
                streamWriter.WriteLine("].");

                streamWriter.Flush();

                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }
    }
}
