﻿using System.Collections.Generic;

namespace Coscine.OrganizationLoader
{
    public class Employee
    {
        public string Surname { get; set; }
        public string PreferredName { get; set; }
        public string ID { get; set; }
        public string RONID { get; set; }
        public List<string> Memberships { get; set; }
        public string DegreePre { get; set; }
        public string DegreePost { get; set; }
        public string Ubpublications { get; set; }
        public List<Role> RoleAssignments { get; set; }
    }
}
