﻿using System.Collections.Generic;

namespace Coscine.OrganizationLoader
{
    public class Organization
    {
        public string OrgId { get; set; }
        public string Name { get; set; }
        public string IKZ { get; set; }
        public string Abbreviation { get; set; }
        public string CMSLink { get; set; }

        public List<Employee> Members { get; set; } = new List<Employee>();
    }
}
