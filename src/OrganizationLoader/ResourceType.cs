﻿namespace Coscine.OrganizationLoader
{
    public class ResourceType
    {
        public string Name { get; set; }
        public int Quota { get; set; }
        public int MaxQuota { get; set; }
        public bool HasQuota { get; set; }
    }
}
