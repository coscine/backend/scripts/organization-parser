﻿using Coscine.Configuration;
using NUnit.Framework;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace Coscine.OrganizationLoader.Tests
{
    [TestFixture]
    public class OrganizationLoaderTests
    {
        readonly IConfiguration configuration = new ConsulConfiguration();

        [Test]
        public void GraphParsingTest()
        {
            var httpClient = new HttpClient();
            // Create a basic Authentication header
            var username = configuration.GetString("coscine/global/organizations/rwth/connection_data/username");
            var password = configuration.GetString("coscine/global/organizations/rwth/connection_data/password");

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));

            string organizationsString;
            string employeesString;

            using (var response = httpClient.GetStringAsync(configuration.GetString("coscine/global/organizations/rwth/files/organizations/link")))
            {
                organizationsString = response.Result;
            }

            using (var response = httpClient.GetStringAsync(configuration.GetString("coscine/global/organizations/rwth/files/employees/link")))
            {
                employeesString = response.Result;
            }

            var organizations = Program.ParseOrganizations(organizationsString);
            var employees = Program.LoadEmployees(employeesString, organizations);

            var ttl = Program.GenerateTurtle(organizations, employees, configuration.GetString("coscine/global/organizations/rwth/ror_url"));
            try
            {
                var graph = new Graph();
                graph.LoadFromString(ttl, new TurtleParser());
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
